// server setup
const express = require("express");
const app = express();
const PORT = 3000;
// routes
const userRouter = require("./routes/users");

app.set('view engine','ejs');

app.get('/', (req, res, next)=>{
    console.log("testing, whats goodie?");
    res.render('index');
})

// middleware for nested routes
app.use('/users', userRouter);

// start the server
app.listen(PORT);