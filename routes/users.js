const express = require('express');
const router = express.Router();

// have the server shoot back a user list
router.get("/", (req, res)=>{
    res.send("User list goes here");
})

module.exports = router